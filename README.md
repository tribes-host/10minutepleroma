# 10minutepleroma.com

A technical demo showcasing automated Pleroma deployments with Kubernetes.
Users may enter a desired subdomain to deploy an ephemeral server which can be accessed for up to 10 minutes.

## Requirements

- A Kubernetes cluster
- A domain name
- [Contour](https://projectcontour.io/) deployed in the cluster, for routing
- [kube-janitor](https://codeberg.org/hjacobs/kube-janitor) deployed in the cluster, for cleaning up resources after 10 minutes
- A wildcard SSL cert

## Installation

TODO

## Local development

To start the Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate the database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
