#!/bin/sh
set -e

echo "-- Waiting for database..."
while ! pg_isready -U ${DB_USER:-postgres} -d $DATABASE_URL -t 1; do
  sleep 1s
done

echo "-- Running migrations..."
mix ecto.migrate

echo "-- Starting!"
mix phx.server
