defmodule TenMinutePleromaWeb.PageLive do
  alias TenMinutePleroma.Deploy
  alias TenMinutePleroma.DeployLog
  alias TenMinutePleroma.EmailList
  alias TenMinutePleroma.Ping
  use TenMinutePleromaWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, email: nil)}
  end

  @impl true
  def handle_event("deploy", %{"subdomain" => subdomain}, socket) do
    url = build_url(subdomain)

    with [ok: _, ok: _, ok: _] <- Deploy.deploy_subdomain(subdomain),
         {:ok, _} <- DeployLog.log(subdomain),
         {:ok, _} <- Ping.wait_until_200(url) do
      Process.send(self(), {:update, subdomain}, []) # Weird hack needed for some reason: https://github.com/phoenixframework/phoenix/issues/4037
      {:noreply, socket}
    else
      {:invalid, ^subdomain} -> {:noreply, put_flash(socket, :error, "Subdomains must use only lowercase letters, numbers, and hyphens. They must begin with a letter.")}

      # Fallback by redirecting to the subdomain.
      # If it validated but got errors, it most likely already exists.
      _ -> {:noreply, redirect(socket, external: build_url(subdomain))}
    end
  end

  @impl true
  def handle_event("email_subscribe", %{"email" => email}, socket) do
    with {:ok, %EmailList{email: ^email}} <- EmailList.subscribe(email) do
      {:noreply, assign(socket, email: email)}
    end
  end

  @impl true
  def handle_info({:update, subdomain}, socket) do
    {:noreply, redirect(socket, external: build_url(subdomain))}
  end

  defp build_url(subdomain) do
    deploy_base_host = Application.get_env(:ten_minute_pleroma, :deploy_base_host)
    "https://#{subdomain}.#{deploy_base_host}"
  end
end
