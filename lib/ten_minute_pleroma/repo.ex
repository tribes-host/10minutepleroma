defmodule TenMinutePleroma.Repo do
  use Ecto.Repo,
    otp_app: :ten_minute_pleroma,
    adapter: Ecto.Adapters.Postgres
end
