defmodule TenMinutePleroma.Deploy do

  def deploy_subdomain(subdomain) when is_binary(subdomain) do
    with {:ok, _} <- validate_subdomain(subdomain),
         {:ok, conn} <- K8s.Conn.lookup(:prod),
         operations when is_list(operations) <- generate_operations(subdomain),
         results <- K8s.Client.async(operations, conn) do
      results
    end
  end

  def validate_subdomain(subdomain) when is_binary(subdomain) do
    # The subdomain itself will be used as the K8s Resource name,
    # so for the sake of simplicity, allow only only valid Resource names
    if Regex.match?(~r/^[a-z]([-a-z0-9]*[a-z0-9])?$/, subdomain) do
      {:ok, subdomain}
    else
      {:invalid, subdomain}
    end
  end

  def validate_subdomain(subdomain), do: {:invalid, subdomain}

  defp generate_operations(subdomain) when is_binary(subdomain) do
    [
      K8s.Client.create(generate_deployment(subdomain)),
      K8s.Client.create(generate_service(subdomain)),
      K8s.Client.create(generate_http_proxy(subdomain))
    ]
  end

  defp generate_service(subdomain) when is_binary(subdomain) do
    %{
      "apiVersion" => "v1",
      "kind" => "Service",
      "metadata" => %{
        "name" => subdomain,
        "namespace" => "default",
        "annotations" => %{
          "janitor/ttl" => "10m" # Clean up after 10 minutes
        }
      },
      "spec" => %{
        "type" => "ClusterIP",
        "selector" => %{
          "host" => get_host(subdomain)
        },
        "ports" => [
          %{
            "port" => 4000
          }
        ]
      }
    }
  end

  defp generate_deployment(subdomain) when is_binary(subdomain) do
    %{
      "apiVersion" => "apps/v1",
      "kind" => "Deployment",
      "metadata" => %{
        "name" => subdomain,
        "namespace" => "default",
        "annotations" => %{
          "janitor/ttl" => "10m" # Clean up after 10 minutes
        }
      },
      "spec" => %{
        "replicas" => 1,
        "selector" => %{
          "matchLabels" => %{
            "host" => get_host(subdomain)
          }
        },
        "template" => %{
          "metadata" => %{
            "labels" => %{
              "host" => get_host(subdomain)
            }
          },
          "spec" => %{
            "containers" => [
              generate_pleroma_container(subdomain),
              generate_postgres_container()
            ]
          }
        }
      }
    }
  end

  defp generate_pleroma_container(subdomain) when is_binary(subdomain) do
    %{
      "name" => "pleroma",
      "image" => "alexgleason/pleroma:latest",
      "ports" => [
        %{
          "containerPort" => 4000
        },
      ],
      "env" => [
        %{
          "name" => "URL_DOMAIN",
          "value" => get_host(subdomain)
        },
        %{
          "name" => "DB_HOST",
          "value" => "localhost"
        },
        %{
          "name" => "DB_PASS",
          "value" => "mysecretpassword"
        }
      ]
    }
  end

  defp generate_postgres_container() do
    %{
      "name" => "postgres",
      "image" => "postgres:13-alpine",
      "ports" => [
        %{
          "containerPort" => 5432
        },
      ],
      "env" => [
        %{
          "name" => "POSTGRES_PASSWORD",
          "value" => "mysecretpassword"
        }
      ]
    }
  end

  defp generate_http_proxy(subdomain) do
    %{
      "apiVersion" => "projectcontour.io/v1",
      "kind" => "HTTPProxy",
      "metadata" => %{
        "name" => subdomain,
        "namespace" => "default",
        "annotations" => %{
          "janitor/ttl" => "10m" # Clean up after 10 minutes
        }
      },
      "spec" => %{
        "routes" => [
          %{
            "conditions" => [
              %{
                "prefix" => "/"
              }
            ],
            "enableWebsockets" => true,
            "services" => [
              %{
                "name" => subdomain,
                "port" => 4000
              }
            ]
          }
        ],
        "virtualhost" => %{
          "fqdn" => get_host(subdomain),
          "tls" => %{
            "secretName" => "tls-secret"
          }
        }
      }
    }
  end

  defp get_host(subdomain) when is_binary(subdomain) do
    subdomain <> "." <> Application.get_env(:ten_minute_pleroma, :deploy_base_host)
  end
end
