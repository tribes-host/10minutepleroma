defmodule TenMinutePleroma.EmailList do
  alias TenMinutePleroma.EmailList
  alias TenMinutePleroma.Repo
  use Ecto.Schema
  import Ecto.Changeset

  # credo:disable-for-next-line Credo.Check.Readability.MaxLineLength
  @email_regex ~r/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/

  schema "email_list" do
    field :email, :string

    timestamps()
  end

  @doc false
  def changeset(email_list, attrs) do
    email_list
    |> cast(attrs, [:email])
    |> validate_format(:email, @email_regex)
    |> validate_required([:email])
    |> unique_constraint(:email)
  end

  def subscribe(email) do
    %EmailList{}
    |> changeset(%{email: email})
    |> Repo.insert()
  end
end
