defmodule TenMinutePleroma.DeployLog do
  alias TenMinutePleroma.DeployLog
  alias TenMinutePleroma.Repo
  use Ecto.Schema
  import Ecto.Changeset

  schema "deploy_log" do
    field :subdomain, :string

    timestamps()
  end

  @doc false
  def changeset(deploy_log, attrs) do
    deploy_log
    |> cast(attrs, [:subdomain])
    |> validate_required([:subdomain])
  end

  def log(subdomain) do
    %DeployLog{}
    |> changeset(%{subdomain: subdomain})
    |> Repo.insert()
  end
end
