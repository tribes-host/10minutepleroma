defmodule TenMinutePleroma.Ping do
  def wait_until_200(url) do
    do_ping(url, nil)
  end

  defp do_ping(_url, {:ok, %HTTPoison.Response{status_code: 200} = response}) do
    {:ok, response}
  end

  defp do_ping(url, _response) do
    :timer.sleep(3 * 1000) # 3s
    response = HTTPoison.get(url)
    do_ping(url, response)
  end
end
