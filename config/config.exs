# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ten_minute_pleroma,
  deploy_base_host: "10minutepleroma.com",
  ecto_repos: [TenMinutePleroma.Repo]

# Configures the endpoint
config :ten_minute_pleroma, TenMinutePleromaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "jlK+U5Kweqf45Mvl3x3kq4oc0l8l6Ek9b0oAxIQRoYIz12Z6/Yhgh6cPdHvw/YvW",
  render_errors: [view: TenMinutePleromaWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: TenMinutePleroma.PubSub,
  live_view: [signing_salt: "+JReiV9K"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Kubernetes
config :k8s,
  clusters: %{
    prod: %{ # <- this can be any name, used to load connections later
      # Path to kube config
      conn: "~/.kube/config"
    }
  }

  # Import environment specific config. This must remain at the bottom
  # of this file so it overrides the configuration defined above.
  import_config "#{Mix.env()}.exs"
