defmodule TenMinutePleroma.Repo.Migrations.CreateDeployLog do
  use Ecto.Migration

  def change do
    create table(:deploy_log) do
      add :subdomain, :string

      timestamps()
    end

  end
end
