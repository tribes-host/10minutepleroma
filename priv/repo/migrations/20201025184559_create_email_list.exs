defmodule TenMinutePleroma.Repo.Migrations.CreateEmailList do
  use Ecto.Migration

  def change do
    create table(:email_list) do
      add :email, :string

      timestamps()
    end

    create unique_index(:email_list, [:email])
  end
end
